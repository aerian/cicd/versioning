# Versionning CI/CD

## Release CI

This project allow to version your code automatically in your project by including
it into your `.gitlab-ci.yml`.

Example:
```
include:
  - project: 'aerian/cicd/versionning'
    ref: main
    file: 'release-ci.yml'

stages:
  - release
```

### Requirements

You will need either the file `Dangerfile` to be present in your project
or as a variable `DANGER_FILE`. Please customize the file based on your needs.

## Automatical version publishing

Please note that this project use semantic-release for auto incrementation of
version.
If you want to build a version, you should use the Angular commit syntax.

More informations at:
https://github.com/semantic-release/semantic-release#commit-message-format
